/*
 * global_settings.h
 *
 *  Created on: Aug 31, 2018
 *      Author: Tomas Jakubik
 */

#ifndef GLOBAL_SETTINGS_H_
#define GLOBAL_SETTINGS_H_

/**
 * @brief Select what is stored to be sent by main.
 *  0 - nothing
 *  1 - fdev data and decided symbols on channel 63
 *  2 - whole packets from main
 *  3 - interrupt times
 */
#define GLOBAL_SEND_STUFF                        2

/**
 * @brief Select how much statistics to measure for interrupt execution time
 *  0 - only count overruns
 *  1 - max and min time
 *  2 - the above and array of last MASK executions
 */
#define GLOBAL_MEASURE_INTERRUPT_TIME            1
#define GLOBAL_MEASURE_INTERRUPT_TIME_MASK       0x1ff

#endif /* GLOBAL_SETTINGS_H_ */
