/*
 * adc.h
 *
 *  Created on: Apr 12, 2018
 *      Author: Tomas Jakubik
 */

#ifndef ADC_H_
#define ADC_H_

#include "LPC43xx.h"

//#define ADC_CALCULATE_PLL_CONSTS	///< When defined, calculate PLL0AUDIO divider constants, otherwise use constants
#ifdef ADC_CALCULATE_PLL_CONSTS
#define ADC_PLL_N     1	///< Predivider, 12/1 = 12
#define ADC_PLL_M     4	///< Multiplier, 12*4 = 48
#define ADC_PLL_P     5	///< Postdivider, 48/5 = 9.6 MHz
#else
#define ADC_PLL_NDEC  0x302	///< Predivider register value
#define ADC_PLL_MDEC  0x3	///< Multiplier register value
#define ADC_PLL_PDEC  0x5	///< Postdivider register value
#define ADC_CLOCK     9600000UL	///< Clock gained by PLL settings above
#endif

#define ADC_SAMPLE_DIVIDER     1	///< Sample when timer is at this (0 means a sample every ADC_CLOCK clock)


typedef struct
{
	uint32_t fifo_overflow;	//Counts FIFO overflows from adc_start()
	uint32_t adc_overflow;	//Counts how many times ADC value was over 2^12 from adc_start()
	uint32_t adc_underflow;	//Counts how many times ADC value was under 0 from adc_start()
} adc_err_cnt_t;
extern adc_err_cnt_t adc_err_cnt;	///< Counts overflows from last adc_start()

/**
 * @brief Set PLL for the ADCHS.
 */
void adc_set_clock();

/**
 * @brief Set ADCHS.
 * @note PLL must be set before by adc_set_clock().
 */
void adc_init();

/**
 * @brief Start continuous conversion.
 */
void adc_start();

/**
 * @brief Stop continuous conversion.
 */
void adc_stop();

#endif /* ADC_H_ */
