/*
 * chproc.h
 *
 *  Created on: Aug 17, 2018
 *      Author: Tomas Jakubik
 *  chproc ~ channel processing
 */

#ifndef CHPROC_H_
#define CHPROC_H_

#include <stdint.h>
#include "global_settings.h"

#define CHPROC_ATAN2_MAX       6	//Calculate maximally this many atan2 functions in one tick
#define CHPROC_ATAN_TYPE       2	//Select which atan2 is used, 1 - approx_atan2, 2 - cordic_atan2

#define CHPROC_N               (CHPROC_ATAN2_MAX+2)	//Number of chproc structures
#define CHPROC_FDEV_MASK       0x3	//Mask of fdevs
#define CHPROC_MAX_DATA_LEN    64	//Maximal length of packet, including length byte
#define CHPROC_PREAM_MATCH_N   10	//Number of symbols, the preamble must be valid
///@note Original syncword #define CHPROC_SYNCWORD_SINGLE        0xa1a581f2ULL
#define CHPROC_SYNCWORD        0xcc03cc33c003ff0cULL	//Syncword spread to 2 symbols per bit

#define CHPROC_EXPIRE_MAG      (16*2)	///< Preamble must be detected in 16 bits [symbols]
#define CHPROC_EXPIRE_PREAM    (3*32*2)	///< Syncword match must occur in 3 * 32 bits [symbols]
#define CHPROC_EXPIRE_SYNC     (CHPROC_MAX_DATA_LEN*8 + 16)	///< Packet can take max data bytes + CRC [bits]

typedef enum
{
	CHPROC_STATE_IDLE  = 0,	///< Idle, waiting for channel assignement
	CHPROC_STATE_MAG   = 1,	///< Magnitude has risen, looking for preamble
	CHPROC_STATE_PREAM = 2,	///< Preamble valid, loking for syncword
	CHPROC_STATE_SYNC  = 3,	///< Syncword caught, receiving data
	CHPROC_STATE_CRC   = 4,	///< Data are received, CRC calculation remains
	CHPROC_STATE_DONE  = 5,	///< Packet received, data are being processed
}chproc_state_t;	///< States of packet recieval

typedef struct
{
	chproc_state_t state;	///< Active state
	uint64_t symbols_sync;	///< Last received symbols in PREAM state
	int32_t last_angle;	///< Last angle sample
	int32_t fdev[CHPROC_FDEV_MASK+1];	///< Frequency deviation
	uint32_t symbols_pream;	///< Last received symbols in MAG state
	uint32_t pream_match_cnt;	///< Counter of preamble matches in MAG state
	uint32_t state_symbols;	///< Number of symbols since state change
	uint32_t packet_symbols;	///< Number of symbols since packet start
	uint32_t channel;	///< Assigned channel
	uint32_t crc_cnt;	///< Counts CRCed bytes
	uint16_t crc;	///< Current CRC value of the packet
	int16_t dc_remove;	///< DC value of the signal to be removed
	uint8_t data[CHPROC_MAX_DATA_LEN+2];	///< Received data + CRC
	uint8_t byte;	///< Received byte


	//TODO: testing variables, remove
	uint32_t crcok;
}chproc_t;	///< Channel processing structure
extern chproc_t chproc[CHPROC_N];	///< Chproc structures

typedef volatile struct
{
	uint32_t atan2_cnt;	///< Count active atan2 calculations in one tick
}chproc_glob_t;	///< Chproc global state variables
extern chproc_glob_t chproc_glob;	///< Chproc state variables

#if GLOBAL_SEND_STUFF == 1
#define FDEV_SIZE    512
extern int16_t fdev[FDEV_SIZE];
extern int16_t data[FDEV_SIZE];
#warning "Temporary"
extern uint32_t fdev_ptr;
extern uint32_t send;
#endif	/*GLOBAL_SEND_STUFF*/

/**
 * @brief Advance channel into CHPROC_STATE_MAG state.
 * @param channel
 */
uint32_t chproc_impulse_power(uint32_t channel);

/**
 * @brief Process one sample of active channels
 * @param fft_out complex fft data for all channels
 * @return number of active chprocs
 */
uint32_t chproc_tick(int16_t* fft_out);

/**
 * @brief Call this function periodically from main to CRC.
 * @return number of packets waiting to be received
 */
uint32_t chproc_pool();

#endif /* CHPROC_H_ */
