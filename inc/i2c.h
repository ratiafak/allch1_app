/*
 * i2c.h
 *
 *  Created on: Apr 5, 2018
 *      Author: Tomas Jakubik
 *    I2C wrapper for librtlsdr.
 */

#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>
#include "LPC43xx.h"	//CMSIS specifications (for LPC_I2C0 and LPC_I2C1)

/**
 * @brief Initialize I2C.
 */
void i2c_init(void* dev);

/**
 * @brief Write data to I2C
 * @param dev I2C device
 * @param addr I2C address aligned to MSb
 * @param buf data buffer
 * @param len length in bytes
 * @return number of bytes transmitted
 */
int i2c_write_fn(void *dev, uint8_t addr, uint8_t *buf, int len);

/**
 * @brief Read data from I2C
 * @param dev I2C device
 * @param addr I2C address aligned to MSb
 * @param buf data buffer
 * @param len length in bytes
 * @return number of bytes read
 */
int i2c_read_fn(void *dev, uint8_t addr, uint8_t *buf, int len);

#endif /* I2C_H_ */
