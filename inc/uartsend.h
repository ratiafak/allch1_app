/*
 * uartsend.h
 *
 *  Created on: Jun 22, 2018
 *      Author: Tomas Jakubik
 */

#ifndef UARTSEND_H_
#define UARTSEND_H_

#include "LPC43xx.h"

void uartsend_init();
void uartsend_string(char* string);
void uartsend_data_as_string(uint8_t* data, uint32_t length);
void uartsend_array(int16_t* array, size_t size);
void uartsend_num_array(int16_t number, int16_t* array, size_t size);
void uartsend_2array(int16_t* array1, int16_t* array2, size_t size);
void uartsend_num_2array(int16_t number, int16_t* array1, int16_t* array2, size_t size);
void uartsend_byte(uint8_t byte);

#endif /* UARTSEND_H_ */
