/*
 * approx_atan2.c
 *
 *  Created on: Jul 13, 2018
 *      Author: Tomas Jakubik
 */

#include "approx_atan2.h"

/**
 * @brief Calculate approximate atan in range between 0 and 1.
 * @param dividend smaller value of the two operands
 * @param divisor larger value of the two operands
 * @return atan
 */
static inline int16_t approx_atan2_poly(uint32_t dividend, uint32_t divisor)
{
	uint32_t quotient = (dividend << 16)/divisor;
	int64_t result;

	result = ((int64_t)quotient)*724190871;	//a*x
	quotient *= quotient;	//x^2
	result -= ((int64_t)quotient)*2826;	//-b*x^2

	return (result >> 32);
}


/**
 * @brief Approximate atan2 from complex number.
 * @note For one octant: x = (smaller << 16)/bigger; angle [-pi=int16_min pi=int16_max] = (x*x*-2826 + x*724190871) >> 32;
 * @param real real part of the complex number
 * @param imag imaginary part of the complex number
 * @return angle in full range of int16_t [-pi=int16_min pi=int16_max]
 */
int16_t approx_atan2(int16_t real, int16_t imag)
{
	int16_t angle;	//Temporary constant added to the polynome
	uint32_t neg = 0;	//Negate sign of the polynome
	int16_t absreal = real;	//Absolute value of real component
	int16_t absimag = imag;	//Absolute value of imaginary component

	if(imag == 0)
	{
		if(real < 0) return 0x8000;	//-2^15 ~ -pi
		else return 0;	//0 ~ 0
	}
	else if(imag < 0)
	{
		absimag *= -1;
		neg ^= 1;
	}

	if(real == 0)
	{
		if(imag < 0) return 0xc000;	//-2^14 ~ -pi/2
		else return 0x4000;	//2^14 ~ pi/2
	}
	else if(real < 0)
	{
		absreal *= -1;
		neg ^= 1;
	}

	if(absreal > absimag)
	{
		if(real < 0) angle = 0x8000;	//-2^15 ~ -pi
		else angle = 0;	//0 ~ 0

		if(neg) return angle - approx_atan2_poly(absimag, absreal);
		else return angle + approx_atan2_poly(absimag, absreal);
	}
	else if(absreal < absimag)
	{
		if(imag < 0) angle = 0xc000;	//-2^14 ~ -pi/2
		else angle = 0x4000;	//2^14 ~ pi/2

		if(neg) return angle + approx_atan2_poly(absreal, absimag);
		else return angle - approx_atan2_poly(absreal, absimag);
	}
	else	//if(absreal == absimag)
	{
		if(real < 0) angle = 0x6000;	//2^14 + 2^13 ~ 3*pi/4
		else angle = 0x2000;	//2^13 ~ pi/4

		if(imag < 0) return -1*angle;
		else return angle;
	}
}
