/*
 * adc.c
 *
 *  Created on: Apr 12, 2018
 *      Author: Tomas Jakubik
 */

#include "adc.h"

adc_err_cnt_t adc_err_cnt;	///< Counts overflows from last adc_start()
static uint32_t adc_clock = 80000000;	///< Clock input to the ADCHS [Hz]

#ifdef ADC_CALCULATE_PLL_CONSTS
#define PLL0_MSEL_MAX (1<<15)
/* multiplier: compute mdec from msel */
unsigned mdec_new(unsigned msel)
{
	unsigned x = 0x4000, im;
	switch(msel)
	{
	case 0:
		return 0xFFFFFFFF;
	case 1:
		return 0x18003;
	case 2:
		return 0x10003;
	default:
		for(im = msel; im <= PLL0_MSEL_MAX; im++)
			x = ((((x ^ x >> 1) & 1) << 14) | (x >> 1)) & 0xFFFF;
		return x;
	}
}

#define PLL0_NSEL_MAX (1<<8)
/* pre-divider: compute ndec from nsel */
unsigned ndec_new(unsigned nsel)
{
	unsigned x = 0x80, in;
	switch(nsel)
	{
	case 0:
		return 0xFFFFFFFF;
	case 1:
		return 0x302;
	case 2:
		return 0x202;
	default:
		for(in = nsel; in <= PLL0_NSEL_MAX; in++)
			x = ((((x ^ x >> 2 ^ x >> 3 ^ x >> 4) & 1) << 7) | (x >> 1)) & 0xFF;
		return x;
	}
}

#define PLL0_PSEL_MAX (1<<5)
/* post-divider: compute pdec from psel */
unsigned pdec_new(unsigned psel)
{
	unsigned x = 0x10, ip;
	switch(psel)
	{
	case 0:
		return 0xFFFFFFFF;
	case 1:
		return 0x62;
	case 2:
		return 0x42;
	default:
		for(ip = psel; ip <= PLL0_PSEL_MAX; ip++)
			x = ((((x ^ x >> 2) & 1) << 4) | (x >> 1)) & 0x3F;
		return x;
	}
}
#endif /*ADC_CALCULATE_PLL_CONSTS*/

/**
 * @brief Set PLL for the ADCHS.
 */
void adc_set_clock()
{
	LPC_CGU->BASE_ADCHS_CLK |= 0x1;	//Power down the ADCHS clock

	//Set PLL
	LPC_CGU->PLL0AUDIO_CTRL = (0x6 << 24) | (0x3 << 13) | (0x1 << 11) | (0x1 << 0);	//PLL off, autoblock, standard mdec, clock by crystal
#ifdef ADC_CALCULATE_PLL_CONSTS
	uint32_t ndec, mdec, pdec;
	ndec = ndec_new(ADC_PLL_N);	//Transform predivider to register value
	mdec = mdec_new(ADC_PLL_M);	//Transform multiplier to register value
	pdec = pdec_new(ADC_PLL_P);	//Transform postdivider to register value
	adc_clock = (12000000 * ADC_PLL_M) / (ADC_PLL_P * ADC_PLL_N);
	LPC_CGU->PLL0AUDIO_MDIV = mdec & 0x1ffff;
	LPC_CGU->PLL0AUDIO_NP_DIV = ((ndec & 0x3ff) << 12) | (pdec & 0x7f);
#else /*ADC_CALCULATE_PLL_CONSTS*/
	adc_clock = ADC_CLOCK;
	LPC_CGU->PLL0AUDIO_MDIV = ADC_PLL_MDEC & 0x1ffff;
	LPC_CGU->PLL0AUDIO_NP_DIV = ((ADC_PLL_NDEC & 0x3ff) << 12) | (ADC_PLL_PDEC & 0x7f);
#endif /*ADC_CALCULATE_PLL_CONSTS*/
	LPC_CGU->PLL0AUDIO_CTRL &= ~0x1;	//PLL power up
	while(!(LPC_CGU->PLL0AUDIO_STAT & 0x1));	//Wait for PLL lock
	LPC_CGU->PLL0AUDIO_CTRL |= (0x1 << 4);	//Clock output enable

	//Enable ADC clocks
	LPC_CGU->BASE_ADCHS_CLK = (0x08 << 24) | (0x1 << 11) | 0x1;	//Set autoblock and PLL0Audio as input
	LPC_CGU->BASE_ADCHS_CLK &= ~0x1;	//Enable the ADCHS clock
	LPC_CCU1->CLK_M4_ADCHS_CFG = 0x1;
	LPC_CCU1->CLK_ADCHS_CFG = 0x1;
}

/**
 * @brief Set ADCHS.
 * @note PLL must be set before by adc_set_clock().
 */
void adc_init()
{
	uint32_t dgec, crs;

	//ADC
	LPC_ADCHS->POWER_CONTROL &= ~((1 << 18) | (1 << 17)); //Turn off
	LPC_ADCHS->FLUSH = 0x1;	//Flush FIFO
	LPC_ADCHS->FIFO_CFG = (8 << 1) | (1 << 0);	//FIFO full above 8 samples, two samples per word
	LPC_ADCHS->DSCR_STS = 0;	//Table 0, descriptor 0
	LPC_ADCHS->POWER_DOWN = 0;	//Unpowerdown
	LPC_ADCHS->CONFIG = (0x90 << 6) | 0x1;	//Software trigger only
	if(adc_clock > 65000000)	//80 MHz clock
	{
		dgec = 0xe;
		crs = 0x4;
	}
	else if(adc_clock > 50000000)	//65 MHz clock
	{
		dgec = 0xf;
		crs = 0x3;
	}
	else	//50, 30 or 20 MHz clock
	{
		dgec = 0x0;
		if(adc_clock > 30000000)	//50 MHz clock
		{
			crs = 0x2;
		}
		else if(adc_clock > 20000000)	//30 MHz clock
		{
			crs = 0x1;
		}
		else	//20 MHz clock
		{
			crs = 0x0;
		}
	}

	LPC_ADCHS->POWER_CONTROL = (0x1 << 16) | (0x1 << 10) | (0x1 << 4) | crs;	//Two's complement output, set speed, both DC biases
	/**
	 * @note The inputs need 100 kOhm to the ground to be able to work with biases ON.
	 * Without 100 kOhm and with biases ON, the differential input has offset of approximately 0x50.
	 * Without biases there is a risk of drift.
	 *
	 * LPC_ADCHS->POWER_CONTROL = (0x1 << 16) | crs;	//Two's complement output, set speed, no DC bias
	 */
	LPC_ADCHS->ADC_SPEED = (dgec << 20) | (dgec << 16) | (dgec << 12) | (dgec << 8) | (dgec << 4) | dgec;	//Speed
	//Measure channel 2 at time 0 and at time 1, then reset timer and continue
	LPC_ADCHS->DESCRIPTOR[0][0] = (ADC_SAMPLE_DIVIDER << 8) | 0x2;
	LPC_ADCHS->DESCRIPTOR[0][1] = (0x1 << 31) | (0x1 << 24) | ((2*ADC_SAMPLE_DIVIDER + 1) << 8) | (0x1 << 6) | 0x2;
	///@note When using one descriptor with match and reset at 0 it was doing weird things.

	LPC_ADCHS->INTS[0].SET_EN = (1 << 6) | (1 << 5) |(1 << 2);	//Set interrupts for FIFO overflow, value overflow and value underflow

	LPC_ADCHS->POWER_CONTROL |= ((1 << 18) | (1 << 17)); //Turn on
}

/**
 * @brief ADCHS interrupt used for overflow error counting.
 */
void M0_SPIFI_OR_VADC_IRQHandler()
{
	if(LPC_ADCHS->INTS->STATUS & (1 << 2))	//FIFO overflow
	{
		LPC_ADCHS->INTS->CLR_STAT = (1 << 2);
		adc_err_cnt.fifo_overflow++;
	}
	if(LPC_ADCHS->INTS->STATUS & (1 << 5))	//Value overflow
	{
		LPC_ADCHS->INTS->CLR_STAT = (1 << 5);
		adc_err_cnt.adc_overflow++;
	}
	if(LPC_ADCHS->INTS->STATUS & (1 << 6))	//Value underflow
	{
		LPC_ADCHS->INTS->CLR_STAT = (1 << 6);
		adc_err_cnt.adc_underflow++;
	}
}

/**
 * @brief Start continuous conversion.
 */
void adc_start()
{
	adc_err_cnt.adc_overflow = 0;	//Reset overflow counters
	adc_err_cnt.adc_underflow = 0;
	adc_err_cnt.fifo_overflow = 0;
	LPC_ADCHS->DESCRIPTOR[0][0] = (LPC_ADCHS->DESCRIPTOR[0][0] & ~(1 << 3));	//Reset halt bit
	LPC_ADCHS->DESCRIPTOR[0][1] = (LPC_ADCHS->DESCRIPTOR[0][1] & ~(1 << 3)) | (1 << 31);	//Reset halt bit and update registers
	LPC_ADCHS->TRIGGER = 0x1;	//Software trigger
}

/**
 * @brief Stop continuous conversion.
 */
void adc_stop()
{
	LPC_ADCHS->DESCRIPTOR[0][0] |= (1 << 3);	//Set halt bit
	LPC_ADCHS->DESCRIPTOR[0][1] |= (1 << 3) | (1 << 31);	//Set halt bit and update registers
}
